function toggleTheme() {
    const form = document.querySelector('.main-form');  // Select the form
    form.classList.toggle("dark-theme");
    
    // Check if dark theme is enabled
    const isDarkTheme = form.classList.contains("dark-theme");
    
    // Save the theme preference to localStorage
    localStorage.setItem("theme", isDarkTheme ? "dark" : "light");
}

// Function to load the saved theme from localStorage
function loadSavedTheme() {
    const savedTheme = localStorage.getItem("theme");
    const form = document.form;
    
    // Apply the saved theme
    if (savedTheme === "dark") {
        form.classList.add("dark-theme");
    }
}

// Load the saved theme on page load
window.onload = loadSavedTheme;